package com.example.android.vremenska;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.android.vremenska.data.WeatherContract;

public class DatabaseHelper {

    public static final int DATE = 1596016800;
    public static final int DEGREES = 35;
    public static final double HUMIDITY = 30;
    public static final double PRESSURE = 13;
    public static final double MAX_TEMP = 35;
    public static final double MIN_TEMP = 23;
    public static final double WIND_SPEED = 12.2;
    public static final double WEATHER_ID = 321;

    public static TableDescription getColumnDetails(SQLiteDatabase db, String table, String column) {
        Cursor cursor = db.rawQuery("pragma table_info(" + table + ");", null);

        while (cursor.moveToNext()) {
            String name = ColumnProperties.getName(cursor);
            if (name.equals(column)) {
                return new TableDescription(name,
                        ColumnProperties.getType(cursor),
                        ColumnProperties.isNotNull(cursor),
                        ColumnProperties.isPrimaryKey(cursor));
            }
        }

        return null;
    }

    static ContentValues createWeatherContentValues() {
        ContentValues values = new ContentValues();
        values.put(WeatherContract.WeatherEntry.COLUMN_DATE, DATE);
        values.put(WeatherContract.WeatherEntry.COLUMN_DEGREES, DEGREES);
        values.put(WeatherContract.WeatherEntry.COLUMN_HUMIDITY, HUMIDITY);
        values.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, PRESSURE);
        values.put(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP, MAX_TEMP);
        values.put(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP, MIN_TEMP);
        values.put(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED, WIND_SPEED);
        values.put(WeatherContract.WeatherEntry.COLUMN_WEATHER_ID, WEATHER_ID);

        return values;
    }


    public static final class ColumnProperties {
        private static final String NAME = "name";
        private static final String TYPE = "type";
        private static final String PRIMARY_KEY = "pk";
        private static final String NOT_NULL = "notnull";

        public static String getName(Cursor cursor) {
            return cursor.getString(cursor.getColumnIndex(NAME));
        }

        public static String getType(Cursor cursor) {
            return cursor.getString(cursor.getColumnIndex(TYPE));
        }

        public static boolean isPrimaryKey(Cursor cursor) {
            return cursor.getInt(cursor.getColumnIndex(PRIMARY_KEY)) == 1;
        }

        public static boolean isNotNull(Cursor cursor) {
            return cursor.getInt(cursor.getColumnIndex(NOT_NULL)) == 1;
        }
    }



    public static final class TableDescription {
            String name;
            String type;
            boolean isNotNull;
            boolean isPrimaryKey;

        private TableDescription(String name, String type, boolean isNotNull, boolean isPrimaryKey) {
            this.name = name;
            this.type = type;
            this.isNotNull = isNotNull;
            this.isPrimaryKey = isPrimaryKey;
        }
    }






}
