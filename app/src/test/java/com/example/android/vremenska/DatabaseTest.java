package com.example.android.vremenska;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.test.core.app.ApplicationProvider;

import com.example.android.vremenska.data.WeatherContract;
import com.example.android.vremenska.data.WeatherDbHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class DatabaseTest {

    @Before
    public void deleteTheDatabase() {
        ApplicationProvider.getApplicationContext().deleteDatabase(WeatherDbHelper.DATABASE_NAME);
    }

    @Test
    public void testCreationOfDb() {

        Context context = ApplicationProvider.getApplicationContext();
        SQLiteDatabase sqLiteDatabase = new WeatherDbHelper(context)
                                            .getWritableDatabase();

        assertTrue(sqLiteDatabase.isOpen());

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'",
                null);


        assertTrue("DB has not been created correctly",
                cursor.moveToFirst());

        cursor.moveToNext();

        assertEquals(WeatherContract.WeatherEntry.TABLE_NAME, cursor.getString(0));
    }

    @Test
    public void testInsertionInWeather() {

        Context context = ApplicationProvider.getApplicationContext();
        SQLiteDatabase sqLiteDatabase = new WeatherDbHelper(context)
                .getWritableDatabase();

        ContentValues contentValues = DatabaseHelper.createWeatherContentValues();

        long rowId = sqLiteDatabase.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, contentValues);

        assertTrue(rowId != -1);

        Cursor weatherCursor = sqLiteDatabase.query(
                WeatherContract.WeatherEntry.TABLE_NAME,  // Table to Query
                null,
                null,
                null,
                null,
                null,
                null
        );

        assertTrue( "Error: No Records", weatherCursor.moveToFirst() );

        assertEquals(DatabaseHelper.DATE,
                weatherCursor.getInt(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_DATE)));
        assertEquals(DatabaseHelper.DEGREES,
                weatherCursor.getInt(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_DEGREES)));
        assertTrue(DatabaseHelper.HUMIDITY ==
                weatherCursor.getDouble(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_HUMIDITY)));
        assertTrue(DatabaseHelper.MAX_TEMP ==
                weatherCursor.getDouble(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP)));
        assertTrue(DatabaseHelper.MIN_TEMP ==
                weatherCursor.getDouble(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP)));
        assertTrue(DatabaseHelper.PRESSURE ==
                weatherCursor.getDouble(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_PRESSURE)));
        assertTrue(DatabaseHelper.WIND_SPEED ==
                weatherCursor.getDouble(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED)));
        assertTrue(DatabaseHelper.WEATHER_ID ==
                weatherCursor.getInt(weatherCursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_WEATHER_ID)));



        assertFalse( "Error: More than one record returned from weather query",
                weatherCursor.moveToNext() );

        weatherCursor.close();
        sqLiteDatabase.close();
    }

    @Test
    public void testDeletion() {

        Context context = ApplicationProvider.getApplicationContext();
        SQLiteDatabase sqLiteDatabase = new WeatherDbHelper(context)
                .getWritableDatabase();

        ContentValues contentValues = DatabaseHelper.createWeatherContentValues();

        long rowId = sqLiteDatabase.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, contentValues);
        assertTrue(rowId != -1);

        sqLiteDatabase.delete(WeatherContract.WeatherEntry.TABLE_NAME, null, null);

        Cursor weatherCursor = sqLiteDatabase.query(
                WeatherContract.WeatherEntry.TABLE_NAME,  // Table to Query
                null,
                null,
                null,
                null,
                null,
                null
        );

        assertFalse( "We didn't deleted everything", weatherCursor.moveToFirst() );

        weatherCursor.close();
        sqLiteDatabase.close();
    }



}
