package com.example.android.vremenska;


import androidx.test.core.app.ApplicationProvider;

import com.example.android.vremenska.data.WeatherDbHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class DatabaseSchemaTest{

    WeatherDbHelper weatherDbHelper;

    @Before
    public void setup() {
        weatherDbHelper = new WeatherDbHelper(ApplicationProvider.getApplicationContext());
    }

    @Test
    public void weatherTableWeatherId() {
        DatabaseHelper.TableDescription weather_id = DatabaseHelper.getColumnDetails(
                weatherDbHelper.getReadableDatabase(),
                "weather",
                "weather_id");

        assertTrue(!weather_id.isPrimaryKey);
        assertEquals(weather_id.name, "weather_id");
        assertEquals(weather_id.type, "INTEGER");
        assertTrue(weather_id.isNotNull);
    }



    @Test
    public void weatherTableMin() {
        DatabaseHelper.TableDescription min = DatabaseHelper.getColumnDetails(
                weatherDbHelper.getReadableDatabase(),
                "weather",
                "min");

        assertFalse(min.isPrimaryKey);
        assertEquals(min.name, "min");
        assertEquals(min.type, "REAL");
        assertTrue(min.isNotNull);

    }

    @Test
    public void weatherTableMax() {
        DatabaseHelper.TableDescription max = DatabaseHelper.getColumnDetails(
                weatherDbHelper.getReadableDatabase(),
                "weather",
                "max");

        assertFalse(max.isPrimaryKey);
        assertEquals(max.name, "max");
        assertEquals(max.type, "REAL");
        assertTrue(max.isNotNull);
    }

    @Test
    public void weatherTableHumidity() {
        DatabaseHelper.TableDescription humidity = DatabaseHelper.getColumnDetails(
                weatherDbHelper.getReadableDatabase(),
                "weather",
                "humidity");

        assertFalse(humidity.isPrimaryKey);
        assertEquals(humidity.name, "humidity");
        assertEquals(humidity.type, "REAL");
        assertTrue(humidity.isNotNull);
    }

    @Test
    public void weatherTablePressure() {
        DatabaseHelper.TableDescription pressure = DatabaseHelper.getColumnDetails(
                weatherDbHelper.getReadableDatabase(),
                "weather",
                "pressure");

        assertFalse(pressure.isPrimaryKey);
        assertEquals(pressure.name, "pressure");
        assertEquals(pressure.type, "REAL");
        assertTrue(pressure.isNotNull);
    }

    @Test
    public void weatherTableWind() {
        DatabaseHelper.TableDescription pressure = DatabaseHelper.getColumnDetails(
                weatherDbHelper.getReadableDatabase(),
                "weather",
                "wind");

        assertFalse(pressure.isPrimaryKey);
        assertEquals(pressure.name, "wind");
        assertEquals(pressure.type, "REAL");
        assertTrue(pressure.isNotNull);
    }

    @Test
    public void weatherTableDegrees() {
        DatabaseHelper.TableDescription degrees = DatabaseHelper.getColumnDetails(
                weatherDbHelper.getReadableDatabase(),
                "weather",
                "degrees");

        assertFalse(degrees.isPrimaryKey);
        assertEquals(degrees.name, "degrees");
        assertEquals(degrees.type, "REAL");
        assertTrue(degrees.isNotNull);
    }


}
