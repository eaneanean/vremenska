package com.example.android.vremenska;

import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.test.core.app.ApplicationProvider;

import com.example.android.vremenska.data.WeatherContract;
import com.example.android.vremenska.data.WeatherDbHelper;
import com.example.android.vremenska.data.WeatherProvider;
import com.example.android.vremenska.utilities.VremenskaDateUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class ContentProviderTest {

    private Context context;

    @Before
    public void setUp() {
        context = ApplicationProvider.getApplicationContext();
        deleteContainerData();
    }

    private void deleteContainerData() {
        context.getContentResolver().delete(
                WeatherContract.WeatherEntry.CONTENT_URI,
                null,
                null
        );
    }

    @Test
    public void testWhetherContentProviderisCorrectlyRegisted() {

        PackageManager packageManager = context.getPackageManager();
        ComponentName componentName = new ComponentName(context.getPackageName(), WeatherProvider.class.getName());

        try {
            ProviderInfo providerInfo = packageManager.getProviderInfo(componentName, 0);
            assertEquals(providerInfo.authority, WeatherContract.CONTENT_AUTHORITY);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            assertFalse(true);
        }

    }

    @Test
    public void insertIntoDatabaseReadWithProvider() {

        WeatherDbHelper weatherDbHelper = new WeatherDbHelper(context);
        SQLiteDatabase sqLiteDatabase = weatherDbHelper.getWritableDatabase();

        ContentValues values = DatabaseHelper.createWeatherContentValues();

        long weatherRowId = sqLiteDatabase.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, values);

        assertTrue("Vnes vo bazata ne e vozmozen", weatherRowId != -1);

        sqLiteDatabase.close();

        Cursor cursor = context.getContentResolver().query(
                WeatherContract.WeatherEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        assertTrue( "Greska, mora da vrati row", cursor.moveToFirst() );

        assertEquals(DatabaseHelper.DATE,
                cursor.getInt(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_DATE)));
        assertEquals(DatabaseHelper.DEGREES,
                cursor.getInt(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_DEGREES)));
        assertTrue(DatabaseHelper.HUMIDITY ==
                cursor.getDouble(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_HUMIDITY)));
        assertTrue(DatabaseHelper.MAX_TEMP ==
                cursor.getDouble(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP)));
        assertTrue(DatabaseHelper.MIN_TEMP ==
                cursor.getDouble(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP)));
        assertTrue(DatabaseHelper.PRESSURE ==
                cursor.getDouble(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_PRESSURE)));
        assertTrue(DatabaseHelper.WIND_SPEED ==
                cursor.getDouble(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED)));
        assertTrue(DatabaseHelper.WEATHER_ID ==
                cursor.getInt(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_WEATHER_ID)));
    }

    @Test
    public void updateDatabaseReadWithProvider() {

        WeatherDbHelper weatherDbHelper = new WeatherDbHelper(context);
        SQLiteDatabase sqLiteDatabase = weatherDbHelper.getWritableDatabase();

        ContentValues values = DatabaseHelper.createWeatherContentValues();

        long weatherRowId = sqLiteDatabase.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, values);

        assertTrue("Vnes vo bazata ne e vozmozen", weatherRowId != -1);

        ContentValues updatedValues = new ContentValues(values);
        updatedValues.put(WeatherContract.WeatherEntry.COLUMN_WEATHER_ID, weatherRowId);
        updatedValues.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, 32);

        String where = WeatherContract.WeatherEntry.COLUMN_WEATHER_ID + "= ?";
        String[] whereArgs = new String[] {String.valueOf(weatherRowId)};

        int rowsAffected = sqLiteDatabase.update(WeatherContract.WeatherEntry.TABLE_NAME,
                updatedValues,
                null,
                null);
        assertEquals(1, rowsAffected);

        sqLiteDatabase.close();

        Cursor cursor = context.getContentResolver().query(
                WeatherContract.WeatherEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        cursor.moveToFirst();

        assertEquals(32,
                cursor.getDouble(cursor.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_PRESSURE)), 0.0);

    }

    @Test
    public void testBulkInsert() {
        WeatherDbHelper weatherDbHelper = new WeatherDbHelper(context);
        SQLiteDatabase sqLiteDatabase = weatherDbHelper.getWritableDatabase();

        ContentValues firstDay = DatabaseHelper.createWeatherContentValues();
        ContentValues secondDay = new ContentValues(firstDay);
        secondDay.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, 40);
        firstDay.put(WeatherContract.WeatherEntry.COLUMN_DATE, VremenskaDateUtils.getNormalizedUtcDateForToday());
        secondDay.put(WeatherContract.WeatherEntry.COLUMN_DATE, VremenskaDateUtils.getNormalizedUtcDateForToday()+ 86400000);
        ContentValues [] contentValues = new ContentValues[2];
        contentValues[0] = firstDay;
        contentValues[1] = secondDay;

        context.getContentResolver().bulkInsert(WeatherContract.WeatherEntry.CONTENT_URI, contentValues);

        Cursor cursor = context.getContentResolver().query(
                WeatherContract.WeatherEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        assertEquals(2, cursor.getCount());


    }








}
