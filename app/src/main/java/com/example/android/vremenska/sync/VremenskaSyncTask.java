package com.example.android.vremenska.sync;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.text.format.DateUtils;

import com.example.android.vremenska.data.VremenskaPreferences;
import com.example.android.vremenska.data.WeatherContract;
import com.example.android.vremenska.utilities.NetworkUtils;
import com.example.android.vremenska.utilities.NotificationUtils;
import com.example.android.vremenska.utilities.OpenWeatherJsonUtils;

import java.net.URL;

public class VremenskaSyncTask {


    synchronized public static void syncWeather(Context context) {

        try {

            URL weatherRequestUrl = NetworkUtils.getUrl(context);


            String jsonWeatherResponse = NetworkUtils.getResponseFromHttpUrl(weatherRequestUrl);


            ContentValues[] weatherValues = OpenWeatherJsonUtils
                    .getWeatherContentValuesFromJson(context, jsonWeatherResponse);


            if (weatherValues != null && weatherValues.length != 0) {

                ContentResolver cr = context.getContentResolver();

                cr.delete(
                        WeatherContract.WeatherEntry.CONTENT_URI,
                        null,
                        null);


                cr.bulkInsert(
                        WeatherContract.WeatherEntry.CONTENT_URI,
                        weatherValues);

                boolean notificationsEnabled = VremenskaPreferences.areNotificationsEnabled(context);


                long timeSinceLastNotification = VremenskaPreferences
                        .getEllapsedTimeSinceLastNotification(context);

                boolean oneDayPassedSinceLastNotification = false;

                if (timeSinceLastNotification >= DateUtils.DAY_IN_MILLIS) {
                    oneDayPassedSinceLastNotification = true;
                }

                if (notificationsEnabled && oneDayPassedSinceLastNotification) {
                    NotificationUtils.notifyUserOfNewWeather(context);
                }

            }

        } catch (Exception e) {
            /* Server probably invalid */
            e.printStackTrace();
        }
    }
}