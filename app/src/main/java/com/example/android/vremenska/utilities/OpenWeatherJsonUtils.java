package com.example.android.vremenska.utilities;

import android.content.ContentValues;
import android.content.Context;

import com.example.android.vremenska.data.VremenskaPreferences;
import com.example.android.vremenska.data.WeatherContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public final class OpenWeatherJsonUtils {

    private static final String DATA = "data";

    private static final String ERROR = "error";

    private static final String LAT = "lat";
    private static final String LON = "lon";

    private static final String PRESSURE = "pres";
    private static final String HUMIDITY = "rh";
    private static final String WINDSPEED = "wind_spd";
    private static final String WIND_DIRECTION = "wind_dir";

    private static final String MAX = "max_temp";
    private static final String MIN = "min_temp";

    private static final String WEATHER = "weather";
    private static final String WEATHER_ICON = "icon";
    private static  final String WEATHER_CODE = "code";


    public static ContentValues[] getWeatherContentValuesFromJson(Context context, String forecastJsonStr)
            throws JSONException {

        JSONObject forecastJson = new JSONObject(forecastJsonStr);


        if (forecastJson.has(ERROR)) {
            throw new NullPointerException("There is an error");
        }

        JSONArray jsonWeatherArray = forecastJson.getJSONArray(DATA);

        double cityLatitude = forecastJson.getDouble(LAT);
        double cityLongitude = forecastJson.getDouble(LON);

        VremenskaPreferences.setLocationDetails(context, cityLatitude, cityLongitude);

        ContentValues[] weatherContentValues = new ContentValues[jsonWeatherArray.length()];

        long normalizedUtcStartDay = VremenskaDateUtils.getNormalizedUtcDateForToday();

        for (int i = 0; i < jsonWeatherArray.length(); i++) {

            long dateTimeMillis;
            double pressure;
            int humidity;
            double windSpeed;
            double windDirection;

            double high;
            double low;

            int weatherId;

            JSONObject dayForecast = jsonWeatherArray.getJSONObject(i);

            dateTimeMillis = normalizedUtcStartDay + VremenskaDateUtils.DAY_IN_MILLIS * i;

            pressure = dayForecast.getDouble(PRESSURE);
            humidity = dayForecast.getInt(HUMIDITY);
            windSpeed = dayForecast.getDouble(WINDSPEED);
            windDirection = dayForecast.getDouble(WIND_DIRECTION);

            JSONObject weatherObject =
                    dayForecast.getJSONObject((WEATHER));

            weatherId = weatherObject.getInt(WEATHER_CODE);

            high = dayForecast.getDouble(MAX);
            low = dayForecast.getDouble(MIN);

            ContentValues weatherValues = new ContentValues();
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DATE, dateTimeMillis);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_HUMIDITY, humidity);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, pressure);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DEGREES, windDirection);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP, high);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP, low);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_WEATHER_ID, weatherId);

            weatherContentValues[i] = weatherValues;
        }

        return weatherContentValues;
    }
}