package com.example.android.vremenska.utilities;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.example.android.vremenska.data.VremenskaPreferences;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * These utilities will be used to communicate with the weather servers.
 */
public final class NetworkUtils {

    private static final String TAG = NetworkUtils.class.getSimpleName();

    private static final String DYNAMIC_WEATHER_URL =
            "https://api.weatherbit.io/v2.0/forecast/daily";

    private static final String STATIC_WEATHER_URL =
            "https://andfun-weather.udacity.com/staticweather";

    private static final String FORECAST_BASE_URL = DYNAMIC_WEATHER_URL;


    private static final int numDays = 14;

    private static final String CITY_PARAM = "city";
    private static final String COUNTRY_PARAM = "country";
    private static final String KEY_PARAM = "key";

    private static final String KEY_VALUE = "c3fe9597fe6a4eb18646b38bc7cc15c8";

    private static final String DAYS_PARAM = "days";


    public static URL getUrl(Context context) {
        String locationQuery = VremenskaPreferences.getPreferredWeatherLocation(context);
        return buildUrlWithLocationQuery(locationQuery);
    }


    private static URL buildUrlWithLocationQuery(String locationQuery) {
        Uri weatherQueryUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                .appendQueryParameter(CITY_PARAM, splitCityAndCountryString(locationQuery)[0])
                .appendQueryParameter(COUNTRY_PARAM, splitCityAndCountryString(locationQuery)[1])
                .appendQueryParameter(DAYS_PARAM, Integer.toString(numDays))
                .appendQueryParameter(KEY_PARAM, KEY_VALUE)
                .build();

        try {
            URL weatherQueryUrl = new URL(weatherQueryUri.toString());
            Log.v(TAG, "URL: " + weatherQueryUrl);
            return weatherQueryUrl;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }


    private static String[] splitCityAndCountryString(String cityAndCountry) {
        return cityAndCountry.split(",");
    }


    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            String response = null;
            if (hasInput) {
                response = scanner.next();
            }
            scanner.close();
            return response;
        } finally {
            urlConnection.disconnect();
        }
    }
}