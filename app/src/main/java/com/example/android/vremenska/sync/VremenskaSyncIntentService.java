package com.example.android.vremenska.sync;

import android.app.IntentService;
import android.content.Intent;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class VremenskaSyncIntentService extends IntentService {

    public VremenskaSyncIntentService() {
        super("VremenskaSyncIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        VremenskaSyncTask.syncWeather(this);
    }
}